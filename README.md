# brave-browser

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

Web browser that blocks ads and trackers by default (binary release)

https://github.com/brave/brave-browser

https://gitlab.manjaro.org/packages/community/brave

<br>

How to clone this repository:
```
git clone https://gitlab.com/azul4/content/web-browsers/brave-browser.git
```

